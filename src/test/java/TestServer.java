import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;

/**
 * Created by ian on 15-10-02.
 */
public class TestServer {

    public static final String SERVER = "localhost";
    public static final int PORT = 9998;
    public static final String PREFIX = "http://" + SERVER + ":" + String.valueOf(PORT);


    /* private helper for makeHttp*Request() */
    private static void copyStreamBuffered(InputStream in, OutputStream out) throws IOException {
        BufferedInputStream inBuf = new BufferedInputStream(in, 1024);
        BufferedOutputStream outBuf = new BufferedOutputStream(out, 1024);
        int i;
        while((i = inBuf.read()) > 0)
            outBuf.write(i);
        outBuf.flush();
    }

    /**
     * Make an HTTP Request
     * @param urlStr URL for request
     * @param method HTTP method
     * @return An HttpResponse with status, headers and response body (if given).
     * @throws IOException
     */
    private static HttpResponse makeHttpJsonRequest(String urlStr, String method) throws IOException {
        return makeHttpJsonRequest(urlStr, method, null);
    }

    /**
     * Make an HTTP Request with JSON body
     * @param urlStr URL for request
     * @param method HTTP method
     * @param requestJson The JSON resquest body
     * @return An HttpResponse with status, headers and response body (if given).
     * @throws IOException
     */
    private static HttpResponse makeHttpJsonRequest(String urlStr, String method, String requestJson) throws IOException {

        // Create URL object for HTTP connection
        URL url = new URL(urlStr);

        // Create HTTP connection from URL
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(method);

        // If the user has specified JSON for the request, configure connection, add content-type header
        // and copy JSON to output stream
        if(requestJson != null) {
            con.setDoOutput(true);
            con.setRequestProperty("content-type", "application/json");
            copyStreamBuffered(new ByteArrayInputStream(requestJson.getBytes()), con.getOutputStream());
        }

        // Retrieve information connection
        Map<String, List<String>> headers = con.getHeaderFields();
        int status = con.getResponseCode();
        String response = null;
        try {
            // copy JSON from output stream
            ByteArrayOutputStream responseBytes = new ByteArrayOutputStream();
            copyStreamBuffered(con.getInputStream(), responseBytes);
            response = new String(responseBytes.toByteArray());
        }
        catch(IOException e) { /* no response body */ }

        return new HttpResponse(status, headers, response);
    }

    // Sample user created during setUp
    private User sampleUser;
    private Defect sampleDefect;

    /**
     * Create a sampleUser user and defect.
     * @throws IOException
     */
    @Before
    public void setUp() throws Exception {

        // in case any data was left over from the last request
        tearDown();

        // Create sample user.
        sampleUser = new User().setName("Bar")
                               .setImageUrl("http://example.com/bar")
                               .setUserType(UserType.DEVELOPER);
        HttpResponse response = makeHttpJsonRequest(PREFIX + "/user/", "POST", sampleUser.toJson());
        if(response.getStatus() == 201)
            sampleUser.setUrl(response.getHeaders().get("Location").get(0)); // Header from POST contains the URL of the new user
        else
            throw new Exception("Server responds with: " + response.toString());

        // Create sample defect.
        sampleDefect = new Defect().setSummary("Unfortunately, Notes has Stopped...")
                                   .setCreated(new Date())
                                   .setCreatedByUrl(sampleUser.getUrl())
                                   .setSeverity(Severity.TRIVIAL)
                                   .setStatus(Status.ACCEPTED)
                                   .setAssignedToUrl(sampleUser.getUrl());
        response = makeHttpJsonRequest(PREFIX + "/defect/", "POST", sampleDefect.toJson());
        if(response.getStatus() == 201)
            sampleDefect.setUrl(response.getHeaders().get("Location").get(0));
        else
            throw new Exception("Server responds with: " + response.toString());
    }

    /**
     * Remove all users and defects from the server: restore server to initial state
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {

        String[] repos = new String[] {"defect", "user"}; // The order matters due to FK contraints

        for(String repo : repos) {
            // Get all items from repo
            HttpResponse response = makeHttpJsonRequest(PREFIX + "/" + repo, "GET");
            if (response.getStatus() != 200)
                throw new IOException("Cannot get /" + repo + " repository. Something is wrong, please restart server.");

            // extract the JSON array from inside the response JSON, it it's not there the repo is empty
            JSONArray items = null;
            try {
                JSONObject root = new JSONObject(new JSONTokener(response.getBody()));
                items = root.getJSONObject("_embedded").getJSONArray(repo);
            } catch (JSONException e) { /* empty repo, or else no item array */ }

            // For non-empty repos, delete all items
            if (items != null) {
                for (int i = 0; i < items.length(); i++) {
                    // the URL is located in the "self" -> "href" field.
                    String href = items.getJSONObject(i).getJSONObject("_links").getJSONObject("self").getString("href");

                    // Delete the item
                    HttpResponse deleteResponse = makeHttpJsonRequest(href, "DELETE");
                    if (deleteResponse.getStatus() != 204)
                        throw new Exception("Could not delete " + href + ". Please restart server.");
                }
            }
        }
    }


    @Test
    public void testUserToJson() {
        // TODO: optional
    }

    @Test
    public void testUserFromJson() {
        // TODO: optional
    }

    @Test
    public void testDefectToJson() {
        // TODO: optional
    }

    @Test
    public void testDefectFromJson() {
        // TODO: optional
    }

    /**
     * Test Requirement 0: can connect to server.
     * @throws IOException
     */
    @Test
    public void testConnection() throws IOException {
        HttpResponse response = makeHttpJsonRequest(PREFIX, "GET");
        assertEquals(200, response.getStatus());
    }

    /**
     * Test: read user
     * @throws IOException
     */
    @Test
    public void testReadUser() throws IOException {
        HttpResponse response = makeHttpJsonRequest(sampleUser.getUrl(), "GET");
        assertEquals(200, response.getStatus());
        User receivedUser = User.fromJson(response.getBody());
        assertEquals(sampleUser, receivedUser);
    }

    /**
     * Test: create user
     * @throws IOException
     */
    @Test
    public void testCreateUser() throws IOException {
        // regular user
        User sampleUser = new User().setName("Foo")
                .setImageUrl("http://example.com/foo")
                .setUserType(UserType.MANAGER);
        HttpResponse response = makeHttpJsonRequest(PREFIX + "/user", "POST", sampleUser.toJson());
        assertEquals(201, response.getStatus());

        // with optional fields
        User sampleUser2 = new User().setName("Quux")
                                     .setUserType(UserType.MANAGER);
        response = makeHttpJsonRequest(PREFIX + "/user", "POST", sampleUser2.toJson());
        assertEquals(201, response.getStatus());
    }


    /**
     * Test: update user
     * @throws IOException
     */
    @Test
    public void testUpdateUser() throws IOException {
        // get sample user
        HttpResponse response = makeHttpJsonRequest(sampleUser.getUrl(), "GET");
        assertEquals(200, response.getStatus());
        User receivedUser = User.fromJson(response.getBody());

        // update user object locally
        receivedUser.setName("Foo")
                .setImageUrl("http://example.com/foo")
                .setUserType(UserType.MANAGER);

        // push updates to server
        response = makeHttpJsonRequest(receivedUser.getUrl(), "PUT", receivedUser.toJson());
        assertEquals(204, response.getStatus());
    }

    /**
     * Test: delete user
     * @throws IOException
     */
    @Test
    public void testDeleteUser() throws IOException {

        // first delete fails due to FK constraint
        HttpResponse response = makeHttpJsonRequest(sampleUser.getUrl(), "DELETE");
        assertEquals(409, response.getStatus());

        makeHttpJsonRequest(sampleDefect.getUrl(), "DELETE");
        response = makeHttpJsonRequest(sampleUser.getUrl(), "DELETE");
        assertEquals(204, response.getStatus());

        // deleted resourcee is no longer on the server
        response = makeHttpJsonRequest(sampleUser.getUrl(), "DELETE");
        assertEquals(404, response.getStatus());

    }

    /**
     * Test: unique names values
     * @throws IOException
     */
    @Test
    public void testUserNameUnique() throws IOException {
        User sampleUser = new User().setName("foo")
                                    .setUserType(UserType.DEVELOPER);

        HttpResponse response = makeHttpJsonRequest(PREFIX + "/user", "POST", sampleUser.toJson());
        assertEquals(201, response.getStatus());

        // second create with same name
        response = makeHttpJsonRequest(PREFIX + "/user", "POST", sampleUser.toJson());
        assertEquals(409, response.getStatus());
    }

    /**
     * Test: usertype values
     * @throws IOException
     */
    @Test
    public void testUserTypeValues() throws IOException {
        int i = 0;
        for(UserType userType : UserType.values()) {
            User user = new User().setName("foo" + String.valueOf(i++))
                                  .setUserType(userType);
            HttpResponse response = makeHttpJsonRequest(PREFIX + "/user", "POST", user.toJson());
            assertEquals(201, response.getStatus());
        }

    }



    /**
     * Test: read defect
     * @throws IOException
     */
    @Test
    public void testReadDefect() throws IOException {
        HttpResponse response = makeHttpJsonRequest(sampleDefect.getUrl(), "GET");
        assertEquals(200, response.getStatus());
        Defect receivedDefect = Defect.fromJson(response.getBody());
        sampleDefect.setCreatedByUrl(sampleDefect.getUrl() + "/createdBy");
        sampleDefect.setAssignedToUrl(sampleDefect.getUrl() + "/assignedTo");
        assertEquals(sampleDefect, receivedDefect);
    }

    /**
     * Test: create defect
     * @throws IOException
     */
    @Test
    public void testCreateDefect() throws IOException {
        // regular user
        Date now = new Date();
        Defect sampleDefect = new Defect().setSummary("And now for something completely different...")
                .setCreated(now)
                .setCreatedByUrl(sampleUser.getUrl())
                .setSeverity(Severity.TRIVIAL)
                .setStatus(Status.CREATED)
                .setModified(new Date(now.getTime() + 1000))
                .setAssignedToUrl(sampleUser.getUrl());
        HttpResponse response = makeHttpJsonRequest(PREFIX + "/defect", "POST", sampleDefect.toJson());
        assertEquals(201, response.getStatus());

        // with optional fields
        sampleDefect = new Defect().setSummary("And now for something completely different...")
                .setCreated(now)
                .setCreatedByUrl(sampleUser.getUrl())
                .setStatus(Status.CREATED);
        response = makeHttpJsonRequest(PREFIX + "/defect", "POST", sampleDefect.toJson());
        assertEquals(201, response.getStatus());
    }


    /**
     * Test: update defect
     * @throws IOException
     */
    @Test
    public void testUpdateDefect() throws IOException {
        // get sample defect
        HttpResponse response = makeHttpJsonRequest(sampleDefect.getUrl(), "GET");
        assertEquals(200, response.getStatus());
        Defect receivedDefect = Defect.fromJson(response.getBody());

        // update defect object locally
        receivedDefect.setModified(new Date())
                .setSummary("And now for something completely different...")
                .setStatus(Status.ACCEPTED)
                .setCreatedByUrl(sampleUser.getUrl())  // TODO: this needed to be reinitalized to the sample user URL:
                                                       //       sending back the received URL ending in "/defect/{id}/createdBy" didn't work
                .setAssignedToUrl(sampleUser.getUrl())
                .setSeverity(Severity.SHOWSTOPPER);

        // push updates to server
        String json = receivedDefect.toJson();
        response = makeHttpJsonRequest(receivedDefect.getUrl(), "PUT", receivedDefect.toJson());
        assertEquals(204, response.getStatus());
    }

    /**
     * Test: delete defect
     * @throws IOException
     */
    @Test
    public void testDeleteDefect() throws IOException {

        // first delete fails due to FK constraint
        makeHttpJsonRequest(sampleDefect.getUrl(), "DELETE");
        HttpResponse response = makeHttpJsonRequest(sampleUser.getUrl(), "DELETE");
        assertEquals(204, response.getStatus());

        // deleted resource is no longer on the server
        response = makeHttpJsonRequest(sampleDefect.getUrl(), "DELETE");
        assertEquals(404, response.getStatus());
    }


    /**
     * Test: status values
     * @throws IOException
     */
    @Test
    public void testStatusValues() throws IOException {
        int i = 0;
        for(Status status : Status.values()) {
            Defect defect = new Defect()
                    .setCreated(new Date())
                    .setCreatedByUrl(sampleUser.getUrl())
                    .setAssignedToUrl(sampleUser.getUrl())  // required for status == ACCEPTED
                    .setStatus(status);
            HttpResponse response = makeHttpJsonRequest(PREFIX + "/defect", "POST", defect.toJson());
            assertEquals(201, response.getStatus());
        }
    }

    /**
     * Test: severity values
     * @throws IOException
     */
    @Test
    public void testSeverityValues() throws IOException {
        int i = 0;
        for(Severity severity : Severity.values()) {
            Defect defect = new Defect()
                    .setCreated(new Date())
                    .setCreatedByUrl(sampleUser.getUrl())
                    .setStatus(Status.CREATED)
                    .setSeverity(severity);
            HttpResponse response = makeHttpJsonRequest(PREFIX + "/defect", "POST", defect.toJson());
            assertEquals(201, response.getStatus());
        }
    }

    /**
     * Test: defect createdBy
     * @throws IOException
     */
    @Test
    public void testDefectCreatedBy() throws IOException {
        HttpResponse response = makeHttpJsonRequest(sampleDefect.getUrl() + "/createdBy", "GET");
        assertEquals(200, response.getStatus());

        User user = User.fromJson(response.getBody());
        assertEquals(sampleUser, user);
    }

    /**
     * Test: defect assignedTo
     * @throws IOException
     */
    @Test
    public void testDefectAssignedTo() throws IOException {
        HttpResponse response = makeHttpJsonRequest(sampleDefect.getUrl() + "/assignedTo", "GET");
        assertEquals(200, response.getStatus());

        User user = User.fromJson(response.getBody());
        assertEquals(sampleUser, user);
    }

    /**
     * Test: user defects values
     * @throws IOException
     */
    @Test
    public void testUserDefectsCreatedBy() throws IOException {

        // make some sample defects
        List<Defect> defects = new ArrayList<>();
        for(int i = 0; i < 3; i++)
            defects.add(new Defect()
                    .setCreated(new Date())
                    .setCreatedByUrl(sampleUser.getUrl())
                    .setStatus(Status.CREATED)
            );

        // create sample defects on server
        for(Defect defect : defects)
            makeHttpJsonRequest(PREFIX + "/defect", "POST", defect.toJson());


        // retrieve all
        HttpResponse response = makeHttpJsonRequest(sampleUser.getUrl() + "/created", "GET");
        assertEquals(200, response.getStatus());

        // parse received defects and make sure each is in the initial list (no assumption on list order)
        List<Defect> receivedDefects = Defect.fromJsonList(response.getBody());
        for(Defect sentDefect : defects) {
            /*boolean found = false;
            for(Defect receivedDefect : receivedDefects)
                if(sentDefect.getUrl().equals(receivedDefect.getUrl()))
                    found = true;
            assertTrue(found);
            */
            assertTrue(receivedDefects.contains(sentDefect));
        }

    }

    /**
     * Test: user defects values
     * @throws IOException
     */
    @Test
    public void testUserDefectsAssignedTo() throws IOException {

        // make some sample defects
        List<Defect> defects = new ArrayList<>();
        for(int i = 0; i < 3; i++)
            defects.add(new Defect()
                    .setCreated(new Date())
                    .setCreatedByUrl(sampleUser.getUrl())
                    .setAssignedToUrl(sampleUser.getUrl())
                    .setStatus(Status.ACCEPTED)
            );

        // create sample defects on server
        for(Defect defect : defects)
            makeHttpJsonRequest(PREFIX + "/defect", "POST", defect.toJson());

        // retrieve all
        HttpResponse response = makeHttpJsonRequest(sampleUser.getUrl() + "/assigned", "GET");
        assertEquals(200, response.getStatus());

        // parse received defects and make sure each is in the initial list (no assumption on list order)
        List<Defect> receivedDefects = Defect.fromJsonList(response.getBody());
        for(Defect sentDefect : defects) {
            /*boolean found = false;
            for(Defect receivedDefect : receivedDefects)
                if(sentDefect.getUrl().equals(receivedDefect.getUrl()))
                    found = true;
            assertTrue(found);*/
            assertTrue(receivedDefects.contains(sentDefect));
        }
    }

    /**
     * Test: user defects values
     * @throws IOException
     */
    @Test
    public void testFindUserByUserType() throws IOException {
        User user1 = new User().setUserType(UserType.CUSTOMER).setName("Foo");
        User user2 = new User().setUserType(UserType.CUSTOMER).setName("Quux");
        makeHttpJsonRequest(PREFIX+"/user", "POST", user1.toJson());
        makeHttpJsonRequest(PREFIX+"/user", "POST", user2.toJson());

        // retrieve all Devlopers
        HttpResponse response = makeHttpJsonRequest(PREFIX + "/user/search/findByUserType?userType=CUSTOMER", "GET");
        assertEquals(200, response.getStatus());

        List<User> users = User.fromJsonList(response.getBody());
        assertTrue(users.contains(user1));
        assertTrue(users.contains(user2));
        assertFalse(users.contains(sampleUser));
    }

    /**
     * Test: user defects values
     * @throws IOException
     */
    @Test
    public void testFindDefectCreatedBetween() throws IOException {
        Date now = new Date();

        Defect defect = new Defect().setStatus(Status.CREATED).setCreatedByUrl(sampleUser.getUrl());
        for(int i=0; i<10; i++) {
            defect.setSummary("defect" + String.valueOf(i))
                    .setCreated( new Date( now.getTime() + i * 1000));
            makeHttpJsonRequest(PREFIX+"/defect", "POST", defect.toJson());
        }

        // user server date format
        SimpleDateFormat format = new SimpleDateFormat(Defect.DATE_FORMAT);

        // retrieve a section of these defects
        String startDate = format.format(new Date(now.getTime() + 3000));
        String endDate = format.format(new Date(now.getTime() + 7000));

        String url = PREFIX + "/defect/search/findByCreatedBetween?start=" + startDate + "&end=" + endDate;
        HttpResponse response = makeHttpJsonRequest(url, "GET");
        assertEquals(200, response.getStatus());

        List<Defect> receivedDefects = Defect.fromJsonList(response.getBody());
        for(int i = 3; i <= 7; i++) {
            String summary = "defect" + String.valueOf(i);
            boolean found = false;
            for(Defect receivedDefect : receivedDefects)
                if(summary.equals(receivedDefect.getSummary()))
                    found = true;
            assertTrue(found);
        }
    }
}
