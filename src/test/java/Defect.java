import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *  Updated with a fix for:
 *
 *   - generate JSON with `assignedTo`/`createdBy` instead of `assignedToURL`/`createdByUrl`
 *   - uses the same date format as the server: yyyy-MM-dd HH:mm:ss
 */
public class Defect {

    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final int DATETIME_EQ_TOLERANCE = 1000;

    private static Gson gson = new GsonBuilder()
            .setDateFormat(DATE_FORMAT)
            .excludeFieldsWithoutExposeAnnotation()
            .create();

    // for a single defect
    private class Links {
        public Href self;
        public Href createdBy;
        public Href assignedTo;
    }
    private class Href {
        public String href;

    }

    // for a list of users
    private class DefectList {
        public Embedded _embedded;
    }
    private class Embedded {
        public Defect[] defect;
    }

    /* Fields */

    private String url;
    @Expose
    private Date created;
    @Expose
    private Date modified;
    @Expose
    private String summary;
    @Expose
    private Status status;
    @Expose
    @SerializedName("assignedTo")
    private String assignedToUrl;
    @Expose
    @SerializedName("createdBy")
    private String createdByUrl;
    @Expose
    private Severity severity;
    private Links _links;


    /* Getters and Setters */

    public String getUrl() {
        return url;
    }

    public Defect setUrl(String url) {
        this.url = url;
        return this;
    }

    public Date getCreated() {
        return created;
    }

    public Defect setCreated(Date created) {
        this.created = created;
        return this;
    }

    public Date getModified() {
        return modified;
    }

    public Defect setModified(Date modified) {
        this.modified = modified;
        return this;
    }

    public String getSummary() {
        return summary;
    }

    public Defect setSummary(String summary) {
        this.summary = summary;
        return this;

    }

    public Status getStatus() {
        return status;
    }

    public Defect setStatus(Status status) {
        this.status = status;
        return this;
    }

    public String getAssignedToUrl() {
        return assignedToUrl;
    }

    public Defect setAssignedToUrl(String assignedToUrl) {
        this.assignedToUrl = assignedToUrl;
        return this;
    }

    public String getCreatedByUrl() {
        return createdByUrl;
    }

    public Defect setCreatedByUrl(String createdByUrl) {
        this.createdByUrl = createdByUrl;
        return this;
    }

    public Severity getSeverity() {
        return severity;
    }

    public Defect setSeverity(Severity severity) {
        this.severity = severity;
        return this;
    }

    public String toJson() {
        return gson.toJson(this);
    }

    public static Defect fromJson(String json) {
        Gson gson = new GsonBuilder().setDateFormat(DATE_FORMAT).create();
        Defect defect = gson.fromJson(json, Defect.class);
        defect.setUrl(defect._links.self.href);
        defect.setAssignedToUrl(defect._links.assignedTo.href);
        defect.setCreatedByUrl(defect._links.createdBy.href);
        return defect;
    }

    public static List<Defect> fromJsonList(String json) {
        Gson gson = new GsonBuilder().setDateFormat(DATE_FORMAT).create();
        DefectList defectList = gson.fromJson(json, DefectList.class);
        List<Defect> defects = new ArrayList<Defect>();
        for(Defect defect : defectList._embedded.defect) {
            defect.setUrl(defect._links.self.href);
            defect.setAssignedToUrl(defect._links.assignedTo.href);
            defect.setCreatedByUrl(defect._links.createdBy.href);
            defects.add(defect);
        }
        return defects;
    }

    @Override
    public String toString() {
        return "Defect{" +
                "url='" + url + '\'' +
                ", created=" + created +
                ", modified=" + modified +
                ", summary='" + summary + '\'' +
                ", status=" + status +
                ", assignedToUrl='" + assignedToUrl + '\'' +
                ", createdByUrl='" + createdByUrl + '\'' +
                ", severity=" + severity +
                ", _links=" + _links +
                '}';
    }

    /* helper for .equals() */
    private static boolean nullOrEqual(Object o1, Object o2) {
        try {
            // returns true if both o1 and o2 are null, or else they are equal
            // note: if o1 == o2 then o1.equals(o2) by properties of equality.
            return (o1 == o2) || (o1.equals(o2));
        }
        catch (NullPointerException e) {
            return false;
        }
    }

    private static boolean nullOrEqualDate(Date d1, Date d2) {
        try {
            // returns true if both o1 and o2 are null, or else they are equal
            // note: if o1 == o2 then o1.equals(o2) by properties of equality.
            return (d1 == d2) || (d1.getTime() - d2.getTime() < DATETIME_EQ_TOLERANCE);
        }
        catch (NullPointerException e) {
            return false;
        }
    }

    // TODO: okay, I'm not sure that this version of equality is acceptable, since I'm ignoring the URLs...
    @Override
    public boolean equals(Object rhsObj) {
        Defect rhs = (Defect) rhsObj;
        return nullOrEqualDate(this.created, rhs.created)
                //&& nullOrEqual(this.createdByUrl, rhs.createdByUrl)   // removed because server sends back an alias
                && nullOrEqualDate(this.modified, rhs.modified)
                && nullOrEqual(this.summary, rhs.summary)
                && nullOrEqual(this.status, rhs.status)
                && nullOrEqual(this.severity, rhs.severity)
                //&& nullOrEqual(this.assignedToUrl, rhs.assignedToUrl)  // same as above
                ;
    }
}
