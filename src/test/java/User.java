import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ian on 15-11-05.
 */
public class User {

    private static Gson gson = new GsonBuilder()
                               .excludeFieldsWithoutExposeAnnotation()
                               .create();

    // for a single user
    private class Links {
        public Self self;
    }
    private class Self {
        public String href;
    }

    // for a list of users
    private class UserList {
        public Embedded _embedded;
    }
    private class Embedded {
        public User[] user;
    }

      /* Fields */

    private String url;
    @Expose
    private String name;
    @Expose
    private String imageUrl;
    @Expose
    private UserType userType;
    private Links _links;

    public User() {
    }

    public User(String name, UserType userType, String imageUrl) {
        this.name = name;
        this.userType = userType;
        this.imageUrl = imageUrl;
    }

    /* Getters and Setters */

    public String getUrl() {
        return url;
    }

    public User setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public User setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public UserType getUserType() {
        return userType;
    }

    public User setUserType(UserType userType) {
        this.userType = userType;
        return this;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public static User fromJson(String json) {
        User user = new Gson().fromJson(json, User.class);
        user.setUrl(user._links.self.href);
        return user;
    }

    public static List<User> fromJsonList(String json) {
        UserList userList = new Gson().fromJson(json, UserList.class);
        List<User> users = new ArrayList<User>();
        for(User user : userList._embedded.user) {
            user.setUrl(user._links.self.href);
            users.add(user);
        }
        return users;
    }

    @Override
    public String toString() {
        return "User{" +
                "url='" + url + '\'' +
                ", name='" + name + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", userType=" + userType +
                '}';
    }

    /* helper for .equals() */
    private static boolean nullOrEqual(Object o1, Object o2) {
        try {
            // returns true if both o1 and o2 are null, or else they are equal
            // note: if o1 == o2 then o1.equals(o2) by properties of equality.
            return (o1 == o2) || (o1.equals(o2));
        }
        catch (NullPointerException e) {
            return false;
        }
    }

    @Override
    public boolean equals(Object rhsObj) {
        User rhs = (User) rhsObj;
        return nullOrEqual(this.name, rhs.name)
                && nullOrEqual(this.imageUrl, rhs.imageUrl)
                && nullOrEqual(this.userType, rhs.userType);
    }
}
